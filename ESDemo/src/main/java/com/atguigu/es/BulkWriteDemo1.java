package com.atguigu.es;

import com.atguigu.es.bean.Employee;
import io.searchbox.client.JestClient;
import io.searchbox.client.JestClientFactory;
import io.searchbox.client.config.HttpClientConfig;
import io.searchbox.core.*;
import org.elasticsearch.action.bulk.BulkRequest;

import java.io.IOException;

public class BulkWriteDemo1 {
    public static void main(String[] args) throws IOException {

        JestClientFactory jestClientFactory = new JestClientFactory();

        HttpClientConfig httpClientConfig = (new HttpClientConfig.Builder("http://hadoop102:9200")).build();

        jestClientFactory.setHttpClientConfig(httpClientConfig);

        JestClient jestClient = jestClientFactory.getObject();

        Employee employee = new Employee("2001", 55, "男", "mike", "飞机", 6666.66);

        Index index = new Index.Builder(employee).index("test").type("emps").id("203").build();

        Delete delete = new Delete.Builder("201").index("test").type("emps").build();

        Bulk bulk = new Bulk.Builder().addAction(index).addAction(delete).build();

        BulkResult execute = jestClient.execute(bulk);

        System.out.println("execute.getResponseCode() = " + execute.getResponseCode());

        jestClient.close();
    }
}

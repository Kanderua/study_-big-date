package com.atguigu.es;

import com.atguigu.es.bean.Employee;
import io.searchbox.client.JestClient;
import io.searchbox.client.JestClientFactory;
import io.searchbox.client.config.HttpClientConfig;
import io.searchbox.core.DocumentResult;
import io.searchbox.core.Index;

import java.io.IOException;

public class WriteDemo2 {
    public static void main(String[] args) throws IOException {

        JestClientFactory jestClientFactory = new JestClientFactory();

        HttpClientConfig httpClientConfig = (new HttpClientConfig.Builder("http://hadoop102:9200")).build();

        jestClientFactory.setHttpClientConfig(httpClientConfig);

        JestClient jestClient = jestClientFactory.getObject();

        Employee employee = new Employee("2001", 55, "男", "mike", "飞机", 6666.66);

        /*String str = "{\n" +
                "  \"empid\": 1015,\n" +
                "  \"age\": 32,\n" +
                "  \"balance\": 3400,\n" +
                "  \"name\": \"Nick\",\n" +
                "  \"gender\": \"男\",\n" +
                "  \"hobby\": \"坐飞机,购物\"\n" +
                "}";*/

        Index index = new Index.Builder(employee).index("test").type("emps").id("202").build();

        DocumentResult execute = jestClient.execute(index);

        System.out.println("execute.getResponseCode() = " + execute.getResponseCode());

        jestClient.close();
    }
}

package com.atguigu.sparkstreaming

import org.apache.kafka.clients.consumer.ConsumerRecord
import org.apache.kafka.common.serialization.StringDeserializer
import org.apache.spark.streaming.dstream.{DStream, InputDStream}
import org.apache.spark.streaming.kafka010.ConsumerStrategies.Subscribe
import org.apache.spark.streaming.kafka010.LocationStrategies.PreferConsistent
import org.apache.spark.streaming.kafka010._
import org.apache.spark.streaming.{Seconds, StreamingContext}

object KafkaDemoExactlyOnceDemo1 {
  def main(args: Array[String]): Unit = {

    val streamingContext = new StreamingContext("local[*]", "app1", Seconds(5))


    val kafkaParams = Map[String, Object](
      "bootstrap.servers" -> "hadoop102:9092",
      "key.deserializer" -> classOf[StringDeserializer],
      "value.deserializer" -> classOf[StringDeserializer],
      "group.id" -> "0726test1",
      "auto.offset.reset" -> "latest",
      "enable.auto.commit" -> "false"//是否自动提交
    )

    val topics = Array("topicA")
//    创建DirectKafkaInputDStream
    val ds:InputDStream[ConsumerRecord[String,String]] = KafkaUtils.createDirectStream[String, String](
      streamingContext,
      PreferConsistent,
      Subscribe[String, String](topics, kafkaParams)
    )

    var ranges:Array[OffsetRange]=null

    val ds1 = ds.transform(rdd => {
      ranges = rdd.asInstanceOf[HasOffsetRanges].offsetRanges
      rdd
    })



    /*
    下面的foreach不满足需求，使用上面的transform

    */

//    ds.foreachRDD{ rdd=>
//      //获取当前批次的偏移量
//      val ranges = rdd.asInstanceOf[HasOffsetRanges].offsetRanges
//
//      //打印上面的数组
//      ranges.foreach(println(_))
//
//      stream.asInstanceOf[CancommitOffsets].commitAsync(offsetRanges)
//    }
    /*
    获取偏移量
    */

    val ds2 = ds1.flatMap(record => {
      record.value().split(" ")
    }).map(word => (word, 1))
      .reduceByKey(_ + _)

    ds2.foreachRDD(rdd=>{

      rdd.foreach(println(_))
      //Executor提交offsets
      ds.asInstanceOf[CanCommitOffsets].commitAsync(ranges)
    })

//    ds2.print(100)

//此句不能写在此处↓
//    ds.asInstanceOf[CanCommitOffsets].commitAsync(ranges)

    streamingContext.start()

    streamingContext.awaitTermination()
  }
}

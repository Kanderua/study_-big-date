package com.atguigu.sparkstreaming

import java.sql.{Connection, PreparedStatement, ResultSet}

import com.atguigu.sparkstreaming.utils.JDBCUtil
import org.apache.kafka.clients.consumer.ConsumerRecord
import org.apache.kafka.common.TopicPartition
import org.apache.kafka.common.serialization.StringDeserializer
import org.apache.spark.rdd.RDD
import org.apache.spark.streaming.dstream.InputDStream
import org.apache.spark.streaming.kafka010.ConsumerStrategies.Subscribe
import org.apache.spark.streaming.kafka010.LocationStrategies.PreferConsistent
import org.apache.spark.streaming.kafka010._
import org.apache.spark.streaming.{Seconds, StreamingContext}

import scala.collection.mutable


object KafkaDemoExactlyOnceDemo4 {

  val groupId="0726test1"
  val topicName="topicA"

  def selectOffsetsFromMysql(groupId:String,topicName:String): Map[TopicPartition,Long]={
    val offsets = new mutable.HashMap[TopicPartition,Long]()

    val sql=
      """
        |select
        |  partitionId,offset
        |from offsets
        |where groupId=? and topic=?
        |
        |
        |""".stripMargin
    var connection:Connection =null
    var ps:PreparedStatement =null
    try {
      connection = JDBCUtil.getConnection()
      ps = connection.prepareStatement(sql)

      ps.setString(1,groupId)
      ps.setString(2,topicName)

      val resultSet:ResultSet = ps.executeQuery()

      while (resultSet.next()){

          val topicPartition=new TopicPartition(topicName,resultSet.getInt("partitionId"))
          offsets.put(topicPartition,resultSet.getLong("offset"))
        }

    }
    catch {
      case  e:Exception=>{
        e.printStackTrace()
        throw new RuntimeException("查询偏移量错误")
      }
    } finally {
      if (ps!=null)
        ps.close()
      if (connection!=null)
        connection.close()
    }
  offsets.toMap
  }

  def writeDataAndOffsetsToMysql(result:Array[(String,Int)],ranges:Array[OffsetRange]): Unit ={
    val sql1=
      """
        |
        |INSERT INTO wordcount VALUES(?,?)
        |ON DUPLICATE KEY UPDATE word=VALUES(word),COUNT=COUNT+VALUES(COUNT)
        |
        |""".stripMargin

    val sql2=
      """
        |
        |INSERT INTO offsets VALUES(?,?,?,?)
        |ON DUPLICATE KEY UPDATE offset=values(offset)
        |
        |""".stripMargin

    var connection:Connection =null
    var ps1:PreparedStatement =null
    var ps2:PreparedStatement =null
    try {
      connection = JDBCUtil.getConnection()

      connection.setAutoCommit(false)
      ps1 = connection.prepareStatement(sql1)
      ps2 = connection.prepareStatement(sql2)

      result.foreach{
        case (word,count)=>{
          ps1.setString(1,word)
          ps1.setLong(2,count)

          ps1.addBatch()
        }
      }

      ranges.foreach(offsetRange=>{
        ps2.setString(1,groupId)
        ps2.setString(2,offsetRange.topic)
        ps2.setInt(3,offsetRange.partition)
        ps2.setLong(4,offsetRange.untilOffset)

        ps2.addBatch()
      })

      val dataResult = ps1.executeBatch()
      val offsetResult = ps2.executeBatch()

      connection.commit()

      println("写入数据成功"+dataResult.size)
      println("写入OffSet成功"+offsetResult.size)
    }
    catch {
      case  e:Exception=>{
        e.printStackTrace()
        connection.rollback()
        throw new RuntimeException("查询偏移量错误")
      }
    } finally {
      if (ps1!=null)
        ps1.close()
      if (ps2!=null)
        ps2.close()
      if (connection!=null)
        connection.close()
    }
  }

  def main(args: Array[String]): Unit = {
    val streamingContext = new StreamingContext("local[*]", "app1", Seconds(5))


    val kafkaParams = Map[String, Object](
      "bootstrap.servers" -> "hadoop102:9092",
      "key.deserializer" -> classOf[StringDeserializer],
      "value.deserializer" -> classOf[StringDeserializer],
      "group.id" -> groupId,
      "auto.offset.reset" -> "earliest",
      "enable.auto.commit" -> "false"
    )

    val topics = Array(topicName)

    val offsets = selectOffsetsFromMysql(groupId,topicName)

//    创建ds
    val ds:InputDStream[ConsumerRecord[String,String]] = KafkaUtils.createDirectStream[String, String](
      streamingContext,
      PreferConsistent,
      Subscribe[String, String](topics, kafkaParams,offsets)
    )

     ds.foreachRDD(rdd => {
       if (!rdd.isEmpty()) {
         val ranges = rdd.asInstanceOf[HasOffsetRanges].offsetRanges

         val rdd1 = rdd.flatMap(_.value().split(" ")).map((_, 1)).reduceByKey(_ + _)
         //      rdd1.foreach(println(_))
         val result = rdd1.collect()

         writeDataAndOffsetsToMysql(result, ranges)
       }
    })

    streamingContext.start()

    streamingContext.awaitTermination()
  }
}

package com.atguigu.springbootdemo.mappers;


import com.atguigu.springbootdemo.bean.Employee;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 *@Repository
 * 1. 标记为和数据库操作的dao对象
 * 2. 自动在容器中创建对象
 * 3. 接口无法实例化，需要使用batis的动态代理技术
 *
 * */

@Repository
public interface EmployeeMapper {

    Employee getEmployeeById(Integer id);

    void insertEmployee(Employee employee);

    void updateEmployee(Employee employee);

    void deleteEmployeeById(Integer id);

    List<Employee> getAll();

}

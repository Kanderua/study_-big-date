package com.atguigu.realtime.app

import com.alibaba.fastjson.JSON
import com.atguigu.gmall.constants.TopicName
import com.atguigu.realtime.app.AlertApp.{appName, batchDuration, context}
import com.atguigu.realtime.bean.{OrderDetail, OrderInfo}
import com.atguigu.realtime.utils.{MyKafkaUtil, RedisUtil}
import org.apache.spark.SparkConf
import org.apache.spark.streaming.kafka010.{CanCommitOffsets, HasOffsetRanges, OffsetRange}
import org.apache.spark.streaming.{Seconds, StreamingContext}

object SaleDetailApp extends BaseApp {
  override val appName: String = "UserApp"
  override val batchDuration: Int = 10

  val groupId="0726gmall"

  def main(args: Array[String]): Unit = {
    val sparkConf: SparkConf = new SparkConf().setMaster("local[*]").setAppName(appName)

    //
    sparkConf.set("es.index.auto.create", "true")
    //
    sparkConf.set("es.nodes","hadoop102")
    sparkConf.set("es.port ","9200")

    context=new StreamingContext(sparkConf,Seconds(batchDuration))

    runApp{
      val ds1 = MyKafkaUtil.getKafkaStream(Array(TopicName.GMALL_ORDER_INFO),context,groupId)
      val ds2 = MyKafkaUtil.getKafkaStream(Array(TopicName.GMALL_ORDER_DETAIL),context,groupId)

      var orderInfoOffsetRanges:Array[OffsetRange]=null
      var orderDetailOffsetRanges:Array[OffsetRange]=null

      val ds3 = ds1.transform(rdd => {
        var orderInfoOffsetRanges = rdd.asInstanceOf[HasOffsetRanges].offsetRanges
        rdd.map(record => {
          val orderInfo = JSON.parseObject(record.value(), classOf[OrderInfo])

          (orderInfo.id, orderInfo)
        })
      })

      val ds4 = ds2.transform(rdd => {

        orderDetailOffsetRanges = rdd.asInstanceOf[HasOffsetRanges].offsetRanges

        rdd.map(record => {
          val orderDetail = JSON.parseObject(record.value(), classOf[OrderDetail])
          (orderDetail.order_id, orderDetail)
        })

      })

      val ds5 = ds4.leftOuterJoin(ds3)

      ds5.print()
    }
  }
}

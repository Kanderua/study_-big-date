package com.atguigu.realtime.app

import org.apache.spark.streaming.StreamingContext

abstract class BaseApp {

  val appName:String

  val batchDuration:Int

  var context:StreamingContext=null

  def runApp(code: => Unit):Unit={
    try {
      code

      context.start()

      context.awaitTermination()
    } catch {
      case e:Exception =>{
        e.printStackTrace()
        throw new RuntimeException("运行时异常")
      }
    }
  }
}

package com.atguigu.realtime.app

import java.sql.{Connection, PreparedStatement, ResultSet}
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

import com.alibaba.fastjson.JSON
import com.atguigu.gmall.constants.TopicName
import com.atguigu.realtime.bean.OrderInfo
import com.atguigu.realtime.utils.{JDBCUtil, MyKafkaUtil}
import org.apache.kafka.common.TopicPartition
import org.apache.spark.streaming.kafka010.{HasOffsetRanges, OffsetRange}
import org.apache.spark.streaming.{Seconds, StreamingContext}

import scala.collection.mutable

object GMVApp extends BaseApp {
  override val appName: String = "GMVApp"
  override val batchDuration: Int = 10

  val groupId="gmall0726"

  def main(args: Array[String]): Unit = {

    context=new StreamingContext("local[*]",appName,Seconds(batchDuration))

    runApp{
      val offsetsMap = selectOffsetsFromMysql(groupId,TopicName.GMALL_ORDER_INFO)

      val ds = MyKafkaUtil.getKafkaStream(Array(TopicName.GMALL_ORDER_INFO),context,groupId,true,offsetsMap)

      ds.foreachRDD(rdd => {
        if (!rdd.isEmpty()){
          val ranges = rdd.asInstanceOf[HasOffsetRanges].offsetRanges

          val rdd1 = rdd.map(record => {

            val formatter1 = DateTimeFormatter.ofPattern("yyyy-MM-dd")
            val formatter2 = DateTimeFormatter.ofPattern("HH")
            val formatter3 = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")

            val orderInfo = JSON.parseObject(record.value(), classOf[OrderInfo])

            val dateTime: LocalDateTime = LocalDateTime.parse(orderInfo.create_time, formatter3)
            orderInfo.create_date = dateTime.format(formatter1)
            orderInfo.create_hour = dateTime.format(formatter2)
            orderInfo
          })

          val rdd2 = rdd1.map(orderInfo => ((orderInfo.create_date, orderInfo.create_hour), orderInfo.total_amount))
            .reduceByKey(_ + _)

          val result = rdd2.collect()

          writeDataAndOffsetsToMysql(result,ranges)

        }
      })
    }
  }

  def writeDataAndOffsetsToMysql(result:Array[((String,String),Double)],ranges:Array[OffsetRange]): Unit ={
    val sql1=
      """
        |
        |INSERT INTO gmvstats VALUES(?,?,?)
        |ON DUPLICATE KEY UPDATE gmv=gmv + VALUES(gmv)
        |
        |""".stripMargin

    val sql2=
      """
        |
        |INSERT INTO offsets VALUES(?,?,?,?)
        |ON DUPLICATE KEY UPDATE offset=values(offset)
        |
        |""".stripMargin

    var connection:Connection =null
    var ps1:PreparedStatement =null
    var ps2:PreparedStatement =null
    try {
      connection = JDBCUtil.getConnection()

      connection.setAutoCommit(false)
      ps1 = connection.prepareStatement(sql1)
      ps2 = connection.prepareStatement(sql2)

      result.foreach{
        case ((date,hour),gmv)=>{
          ps1.setString(1,date)
          ps1.setString(2,hour)
          ps1.setDouble(3,gmv)

          ps1.addBatch()
        }
      }

      ranges.foreach(offsetRange=>{
        ps2.setString(1,groupId)
        ps2.setString(2,offsetRange.topic)
        ps2.setInt(3,offsetRange.partition)
        ps2.setLong(4,offsetRange.untilOffset)

        ps2.addBatch()
      })

      val dataResult = ps1.executeBatch()
      val offsetResult = ps2.executeBatch()

      connection.commit()

      println("写入数据成功"+dataResult.size)
      println("写入OffSet成功"+offsetResult.size)
    }
    catch {
      case  e:Exception=>{
        e.printStackTrace()
        connection.rollback()
        throw new RuntimeException("查询偏移量错误")
      }
    } finally {
      if (ps1!=null)
        ps1.close()
      if (ps2!=null)
        ps2.close()
      if (connection!=null)
        connection.close()
    }
  }


  def selectOffsetsFromMysql(groupId:String,topicName:String): Map[TopicPartition,Long]={
    val offsets = new mutable.HashMap[TopicPartition,Long]()

    val sql=
      """
        |select
        |  partitionId,offset
        |from offsets
        |where groupId=? and topic=?
        |
        |
        |""".stripMargin
    var connection:Connection =null
    var ps:PreparedStatement =null
    try {
      connection = JDBCUtil.getConnection()
      ps = connection.prepareStatement(sql)

      ps.setString(1,groupId)
      ps.setString(2,topicName)

      val resultSet:ResultSet = ps.executeQuery()

      while (resultSet.next()){

        val topicPartition=new TopicPartition(topicName,resultSet.getInt("partitionId"))
        offsets.put(topicPartition,resultSet.getLong("offset"))
      }

    }
    catch {
      case  e:Exception=>{
        e.printStackTrace()
        throw new RuntimeException("查询偏移量错误")
      }
    } finally {
      if (ps!=null)
        ps.close()
      if (connection!=null)
        connection.close()
    }
    offsets.toMap
  }


}

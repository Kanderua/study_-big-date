package com.atguigu.realtime.app

import java.time.{LocalDate, LocalDateTime}
import java.time.format.DateTimeFormatter
import java.util

import com.alibaba.fastjson.{JSON, JSONObject}
import com.atguigu.gmall.constants.TopicName
import com.atguigu.realtime.app.AlertApp.{appName, batchDuration, context}
import com.atguigu.realtime.bean.{OrderDetail, OrderInfo, SaleDetail, UserInfo}
import com.atguigu.realtime.utils.{MyKafkaUtil, RedisUtil}
import com.google.gson.Gson
import org.apache.kafka.clients.consumer.ConsumerRecord
import org.apache.spark.SparkConf
import org.apache.spark.rdd.RDD
import org.apache.spark.streaming.dstream.{DStream, InputDStream}
import org.apache.spark.streaming.kafka010.{CanCommitOffsets, HasOffsetRanges, OffsetRange}
import org.apache.spark.streaming.{Seconds, StreamingContext}
import redis.clients.jedis.Jedis

import scala.collection.mutable.ListBuffer
import org.elasticsearch.spark._


object SaleDetailApp2 extends BaseApp {
  override val appName: String = "SaleDetailApp"
  override val batchDuration: Int = 10

  val groupId="0726gmall"

  def main(args: Array[String]): Unit = {
    val sparkConf: SparkConf = new SparkConf().setMaster("local[*]").setAppName(appName)

    //
    sparkConf.set("es.index.auto.create", "true")
    //
    sparkConf.set("es.nodes","hadoop102")
    sparkConf.set("es.port ","9200")

    context=new StreamingContext(sparkConf,Seconds(batchDuration))

    runApp{
      val ds1 = MyKafkaUtil.getKafkaStream(Array(TopicName.GMALL_ORDER_INFO),context,groupId)
      val ds2 = MyKafkaUtil.getKafkaStream(Array(TopicName.GMALL_ORDER_DETAIL),context,groupId)

      var orderInfoOffsetRanges:Array[OffsetRange]=null
      var orderDetailOffsetRanges:Array[OffsetRange]=null

      val ds3 = ds1.transform(rdd => {
        orderInfoOffsetRanges = rdd.asInstanceOf[HasOffsetRanges].offsetRanges
        rdd.map(record => {
          val orderInfo = JSON.parseObject(record.value(), classOf[OrderInfo])

          val formatter1: DateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd")
          val formatter3: DateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")

          val dateTime: LocalDateTime = LocalDateTime.parse(orderInfo.create_time,formatter3)
          orderInfo.create_date = dateTime.format(formatter1)

          (orderInfo.id, orderInfo)
        })
      })

      val ds4 = ds2.transform(rdd => {

        orderDetailOffsetRanges = rdd.asInstanceOf[HasOffsetRanges].offsetRanges

        rdd.map(record => {
          val orderDetail = JSON.parseObject(record.value(), classOf[OrderDetail])
          (orderDetail.order_id, orderDetail)
        })

      })

      val ds5: DStream[(String, (Option[OrderInfo], Option[OrderDetail]))] = ds3.fullOuterJoin(ds4)

      val ds6 = ds5.mapPartitions(partition => {
        val jedis = RedisUtil.getJedisClient()

        val saleDetails = new ListBuffer[SaleDetail]

        partition.foreach {

          case (orderId, (orderInfoOption, orderDetailOption)) => {
            val gson = new Gson()
            if (orderInfoOption != None) {

              val oi = orderInfoOption.get

              if (orderDetailOption != None) {

                val od = orderDetailOption.get

                val saleDetail = new SaleDetail(oi, od)

                saleDetails.append(saleDetail)
              }

              val ods = jedis.smembers("orderDetail:" + orderId)

              ods.forEach(odStr => {
                val orderDetail = JSON.parseObject(odStr, classOf[OrderDetail])

                val saleDetail = new SaleDetail(oi, orderDetail)

                saleDetails.append(saleDetail)
              })

              jedis.setex("orderInfo:" + orderId, 5 * 60 * 3, gson.toJson(oi))
            } else {
              val od = orderDetailOption.get
              val str = jedis.get("orderInfo:" + od.order_id)

              if (str != null) {

                val oi = JSON.parseObject(str, classOf[OrderInfo])

                val saleDetail = new SaleDetail(oi, od)

                saleDetails.append(saleDetail)
              } else {
                jedis.sadd("orderDetail:" + orderId, gson.toJson(od))
                jedis.expire("orderDetail:" + orderId, 5 * 60 * 3)
              }

            }
          }
        }

        jedis.close()
        saleDetails.toIterator
      })

      val ds7 = ds6.mapPartitions(partition => {

        val jedis = RedisUtil.getJedisClient()

        val iterator = partition.map(saleDetail => {
          val str = jedis.get("userinfo:" + saleDetail.user_id)

          if (str != null) {
            val userInfo = JSON.parseObject(str, classOf[UserInfo])

            saleDetail.mergeUserInfo(userInfo)

          }
          saleDetail
        })

        jedis.close()

        iterator
      })

      ds7.foreachRDD(rdd=>{

        rdd.cache()

        println("写入ES:"+rdd.count())

        rdd.saveToEs("gmall2021_sale_detail"+LocalDate.now()+"/_doc",Map("es.mapping.id"->"order_detail_id"))

        ds1.asInstanceOf[CanCommitOffsets].commitAsync(orderInfoOffsetRanges)
        ds2.asInstanceOf[CanCommitOffsets].commitAsync(orderDetailOffsetRanges)

      })

    }
  }
}








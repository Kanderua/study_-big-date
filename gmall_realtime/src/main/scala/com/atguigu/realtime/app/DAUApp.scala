package com.atguigu.realtime.app

import java.time.format.DateTimeFormatter
import java.time.{Instant, LocalDateTime, ZoneId}

import com.alibaba.fastjson.{JSON, JSONObject}
import com.atguigu.gmall.constants.TopicName
import com.atguigu.realtime.bean.{StartLogInfo, StartUpLog}
import com.atguigu.realtime.utils.{MyKafkaUtil, RedisUtil}
import org.apache.hadoop.hbase.HBaseConfiguration
import org.apache.kafka.clients.consumer.ConsumerRecord
import org.apache.spark.rdd.RDD
import org.apache.spark.streaming.dstream.{DStream, InputDStream}
import org.apache.spark.streaming.kafka010.{CanCommitOffsets, HasOffsetRanges, OffsetRange}
import org.apache.spark.streaming.{Seconds, StreamingContext}
import redis.clients.jedis.Jedis

import org.apache.phoenix.spark._

object DAUApp extends BaseApp {
  override val appName: String = "DAUApp"
  override val batchDuration: Int = 10

  val groupId="gmall0726"

  def parseRecordToStartUpLog(rdd: RDD[ConsumerRecord[String, String]]): RDD[StartUpLog] = {
    rdd.map(record=>{
      val jSONObject=JSON.parseObject(record.value())
      val commonStr=jSONObject.getString("common")
      val startUpLog=JSON.parseObject(commonStr,classOf[StartUpLog])

      val startLogInfo=JSON.parseObject(jSONObject.getString("start"),classOf[StartLogInfo])
      startUpLog.mergeStartInfo(startLogInfo)

      startUpLog.ts=jSONObject.getLong("ts")
      val dateTime=LocalDateTime.ofInstant(Instant.ofEpochMilli(startUpLog.ts),ZoneId.of("Asia/Shanghai"))
      val formatter1=DateTimeFormatter.ofPattern("yyyy-MM-dd")
      val formatter2=DateTimeFormatter.ofPattern("HH")
      startUpLog.logDate=dateTime.format((formatter1))
      startUpLog.logHour=dateTime.format((formatter2))
      startUpLog
    })
  }

  def removeDuplicateLogInCurrentBatch(rdd: RDD[StartUpLog]): RDD[StartUpLog] = {
//    val rdd1 = rdd.map(log => (log.mid,log))
//    val rdd2 = rdd1.groupByKey()
//    val rdd3 = rdd2.flatMap {
//      case (mid_id, logs) => {
//        logs.toList.sortBy(_.ts).take(1)
//      }
//    }
//上面注释的简洁版
    val value = rdd.map(log => (log.mid, log))
      .groupByKey()
      .flatMap {
        case (mid_id, logs) => {
          logs.toList.sortBy(_.ts).take(1)
        }
      }
    value
  }

  def removeDuplicateLogFromHistoryBatch(rdd: RDD[StartUpLog]): RDD[StartUpLog] = {

    val rdd1 = rdd.mapPartitions(partition => {

      val jedis = RedisUtil.getJedisClient()

      val filteredLogs = partition.filter(log => !jedis.sismember("DAU:" + log.logDate, log.mid))

      jedis.close()
      filteredLogs
    })
    rdd1
  }

  def main(args: Array[String]): Unit = {
    context=new StreamingContext("local[*]",appName,Seconds(batchDuration))

    runApp{
      val ds=MyKafkaUtil.getKafkaStream(Array(TopicName.STARTUP_LOG),context,groupId)


      ds.foreachRDD(rdd=>{
        if (!rdd.isEmpty()) {


          val ranges = rdd.asInstanceOf[HasOffsetRanges].offsetRanges

          val rdd1 = parseRecordToStartUpLog(rdd)

          val rdd2 = removeDuplicateLogInCurrentBatch(rdd1)

          val rdd3 = removeDuplicateLogFromHistoryBatch(rdd2)

          rdd3.cache()

          println("即将写入的数据条数：" + rdd3.count())

          rdd3.saveToPhoenix(
            "GMALL2021_STARTUPLOG",
            Seq("AR", "BA", "CH", "IS_NEW", "MD", "MID", "OS", "UID", "VC", "ENTRY", "LOADING_TIME", "OPEN_AD_ID", "OPEN_AD_MS", "OPEN_AD_SKIP_MS", "LOGDATE", "LOGHOUR", "TS"),
            HBaseConfiguration.create(),
            Some("hadoop102:2181")
          )
          rdd3.foreachPartition(partition => {
            val jedis: Jedis = RedisUtil.getJedisClient()

            partition.foreach(log => jedis.sadd("DAU:" + log.logDate, log.mid))

            jedis.close()
          })

          ds.asInstanceOf[CanCommitOffsets].commitAsync(ranges)
        }
      })
    }
  }
}

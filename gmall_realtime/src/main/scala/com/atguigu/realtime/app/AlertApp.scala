package com.atguigu.realtime.app

import java.time.{Instant, LocalDate, LocalDateTime, ZoneId}
import java.time.format.DateTimeFormatter
import java.util

import com.alibaba.fastjson.{JSON, JSONObject}
import com.atguigu.gmall.constants.TopicName
import com.atguigu.realtime.app.DAUApp.{context, groupId}
import com.atguigu.realtime.app.GMVApp.{appName, batchDuration, context}
import com.atguigu.realtime.bean.{Action, ActionsLog, CommonInfo, CouponAlertInfo}
import com.atguigu.realtime.utils.MyKafkaUtil
import org.apache.kafka.clients.consumer.ConsumerRecord
import org.apache.spark.SparkConf
import org.apache.spark.streaming.dstream.{DStream, InputDStream}
import org.apache.spark.streaming.kafka010.{CanCommitOffsets, HasOffsetRanges, OffsetRange}
import org.apache.spark.streaming.{Minutes, Seconds, StreamingContext}

import scala.collection.mutable
import scala.collection.mutable.ListBuffer
import scala.util.control.Breaks

//
//
import collection.JavaConverters._


//
import org.elasticsearch.spark._

/**
 *
 *
 */
object AlertApp extends  BaseApp {
  override val appName: String = "AlertApp"
  override val batchDuration: Int = 10

  val groupId="gmall0726"

  def main(args: Array[String]): Unit = {

    val sparkConf: SparkConf = new SparkConf().setMaster("local[*]").setAppName(appName)

    //
    sparkConf.set("es.index.auto.create", "true")
    //
    sparkConf.set("es.nodes","hadoop102")
    sparkConf.set("es.port ","9200")

    context=new StreamingContext(sparkConf,Seconds(batchDuration))

    runApp{

      // ①
      val ds: InputDStream[ConsumerRecord[String, String]] = MyKafkaUtil.getKafkaStream(Array(TopicName.ACTIONS_LOG), context, groupId)

      var ranges: Array[OffsetRange] = null

      // ②
      val ds1: DStream[ActionsLog] = ds.transform(rdd => {

        //
        ranges = rdd.asInstanceOf[HasOffsetRanges].offsetRanges

        rdd.map(record => {

          val jSONObject: JSONObject = JSON.parseObject(record.value())

          val commonInfo: CommonInfo = JSON.parseObject(jSONObject.getString("common"), classOf[CommonInfo])


          //
          val actions: List[Action] = JSON.parseArray(jSONObject.getString("actions"), classOf[Action]).asScala.toList

          ActionsLog(actions, jSONObject.getLong("ts"), commonInfo)

        })

      })

      // ③
      val ds2: DStream[((String, Long), Iterable[ActionsLog])] = ds1.window(Minutes(5),Seconds(30))
        .map(actionsLog => ((actionsLog.common.mid, actionsLog.common.uid), actionsLog))
        .groupByKey()


      // ④
      //
      val ds3: DStream[((String, Long), Iterable[ActionsLog])] = ds2.filter {

        //
        case ((mid, uid), actionsLogs) => {

          var ifNeedAlert = false

          Breaks.breakable {

            actionsLogs.foreach(actionsLog => {

              actionsLog.actions.foreach(action => {

                if ("trade_add_address".equals(action.action_id)) {

                  ifNeedAlert = true

                  //
                  Breaks.break
                }

              })

            })

          }

          ifNeedAlert

        }
      }

      // ⑤
      val ds4: DStream[(String, Iterable[Iterable[ActionsLog]])] = ds3.map {
        case ((mid, uid), actionsLogs) => (mid, actionsLogs)
      }.groupByKey()


      //
      //
      //
      val ds5: DStream[(String, Iterable[ActionsLog])] = ds4.filter(_._2.size >= 2)
        .mapValues(_.flatten)

      // ⑥
      val ds6: DStream[CouponAlertInfo] = ds5.map {
        case (mid, actionsLogs) => {

          var uids: mutable.Set[String] = new mutable.HashSet[String]()
          var itemIds: mutable.Set[String] = new mutable.HashSet[String]()
          var events: ListBuffer[String] = new ListBuffer[String]

          actionsLogs.foreach(actionsLog => {

            uids.add(actionsLog.common.uid.toString)

            actionsLog.actions.foreach(action => {

              events.append(action.action_id)

              if ("favor_add".equals(action.action_id)) {

                itemIds.add(action.item)

              }

            })

          })

          /*











           */
          val formatter: DateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm")

          val ts: Long = System.currentTimeMillis()

          val time: LocalDateTime = LocalDateTime.ofInstant(Instant.ofEpochMilli(ts), ZoneId.of("Asia/Shanghai"))
          val minute: String = time.format(formatter)

          CouponAlertInfo(mid + "_" + minute, uids, itemIds, events, ts)
        }
      }


      // ⑦
      ds6.foreachRDD(rdd => {

        rdd.cache()

        println("即将写入:"+rdd.count())
        //
        //
        //
        rdd.saveToEs("gmall_coupon_alert"+ LocalDate.now + "/_doc" ,Map("es.mapping.id" -> "id") )

        //
        ds.asInstanceOf[CanCommitOffsets].commitAsync(ranges)

      })


    }

  }
}

package com.atguigu.realtime.app


import com.alibaba.fastjson.{JSON, JSONObject}
import com.atguigu.gmall.constants.TopicName
import com.atguigu.realtime.utils.{MyKafkaUtil, RedisUtil}
import net.minidev.json.JSONObject
import org.apache.spark.streaming.kafka010.{CanCommitOffsets, HasOffsetRanges}
import org.apache.spark.streaming.{Seconds, StreamingContext}

object UserApp extends BaseApp {
  override val appName: String = "UserApp"
  override val batchDuration: Int = 10

  val groupId="0726gmall"

  def main(args: Array[String]): Unit = {
    context = new StreamingContext("local[*]",appName,Seconds(batchDuration))
    runApp{
      val ds = MyKafkaUtil.getKafkaStream(Array(TopicName.GMALL_USER_INFO),context,groupId)

      ds.foreachRDD(rdd=>{

        if (!rdd.isEmpty()){

          val ranges = rdd.asInstanceOf[HasOffsetRanges].offsetRanges

          val rdd1 = rdd.map(record=>record.value())

          println("即将打印："+rdd1.count())

          rdd1.foreachPartition(partition=>{

            val jedis = RedisUtil.getJedisClient()

            partition.foreach(userInfoJsonStr=>{

              val jSONObjedt=JSON.parseObject(userInfoJsonStr)

              jedis.set("userinfo:"+jSONObjedt.getString("id"),userInfoJsonStr)
            })
            jedis.close()
          })

          ds.asInstanceOf[CanCommitOffsets].commitAsync(ranges)
        }
      })
    }
  }
}

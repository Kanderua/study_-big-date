package com.atguigu.realtime.test
//测试
import java.time.{Instant, LocalDateTime, ZoneId}
import java.time.format.DateTimeFormatter

import com.alibaba.fastjson.{JSON, JSONObject}
import com.atguigu.realtime.bean.{StartLogInfo, StartUpLog}


object StartUpLogParse{
  def main(args: Array[String]): Unit = {
    val str =
      """
        |
        |{
        |  "common": {
        |    "ar": "440000",
        |    "ba": "Xiaomi",
        |    "ch": "web",
        |    "is_new": "1",
        |    "md": "Xiaomi 10 Pro ",
        |    "mid": "mid_115",
        |    "os": "Android 9.0",
        |    "uid": "636",
        |    "vc": "v2.1.134"
        |  },
        |  "start": {
        |    "entry": "icon",
        |    "loading_time": 12363,
        |    "open_ad_id": 14,
        |    "open_ad_ms": 7241,
        |    "open_ad_skip_ms": 0
        |  },
        |  "ts": 1638147218000
        |}
        |
        |
        |""".stripMargin


    //将数据转换为json格式

    val jSONObject=JSON.parseObject(str)
    //提取common部分
    val commonStr=jSONObject.getString("common")

    val startUpLog=JSON.parseObject(commonStr,classOf[StartUpLog])

    println(startUpLog)


    //提取start部分
    val startLogInfo=JSON.parseObject(jSONObject.getString("start"),classOf[StartLogInfo])

    startUpLog.mergeStartInfo(startLogInfo)
    println(startUpLog)


    //添加时间
    startUpLog.ts=jSONObject.getLong("ts")

    val dateTime=LocalDateTime.ofInstant(Instant.ofEpochMilli(startUpLog.ts),ZoneId.of("Asia/Shanghai"))
    //将时间戳进行转换
    val formatter1=DateTimeFormatter.ofPattern("yyyy-MM-dd")
    val formatter2=DateTimeFormatter.ofPattern("HH")

    startUpLog.logDate=dateTime.format((formatter1))
    startUpLog.logHour=dateTime.format((formatter2))



    println(startUpLog)

  }
}
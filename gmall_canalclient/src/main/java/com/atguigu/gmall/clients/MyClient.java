package com.atguigu.gmall.clients;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.otter.canal.client.CanalConnector;
import com.alibaba.otter.canal.client.CanalConnectors;
import com.alibaba.otter.canal.protocol.CanalEntry;
import com.alibaba.otter.canal.protocol.Message;
import com.atguigu.gmall.constants.TopicName;
import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;

import java.net.InetSocketAddress;
import java.util.List;

public class MyClient {
    public static void main(String[] args) throws InterruptedException, InvalidProtocolBufferException {
        CanalConnector canalConnector = CanalConnectors.newSingleConnector(new InetSocketAddress("hadoop103", 11111),
                "example", null, null);

        canalConnector.connect();

        canalConnector.subscribe("gmall0726.order_info");

        while (true){
            Message message = canalConnector.get(100);
                if (message.getId() == -1){
                    System.out.println("没有数据生成");
                    Thread.sleep(6666);
                    continue;
                }
//            System.out.println("接收到的数据：\n"+message);

            List<CanalEntry.Entry> entries = message.getEntries();

            for (CanalEntry.Entry entry: entries
                 ) {
                if (entry.getEntryType()==CanalEntry.EntryType.ROWDATA){
                    parseData(entry.getStoreValue());
                }
            }
        }



    }

    private static void parseData(ByteString storeValue) throws InvalidProtocolBufferException {
        CanalEntry.RowChange rowChange = CanalEntry.RowChange.parseFrom(storeValue);

        if (rowChange.getEventType()==CanalEntry.EventType.INSERT){
            List<CanalEntry.RowData> rowDatasList = rowChange.getRowDatasList();

            for (CanalEntry.RowData rowData:rowDatasList             ) {

                JSONObject jsonObject = new JSONObject();

                List<CanalEntry.Column> afterColumnsList = rowData.getAfterColumnsList();


                for (CanalEntry.Column column:afterColumnsList                     ) {
                    jsonObject.put(column.getName(),column.getValue());
                }
//                System.out.println(jsonObject);
                MyProducer.sendData(TopicName.GMALL_ORDER_INFO,jsonObject.toJSONString());


            }
        }


    }
}

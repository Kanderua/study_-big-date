package com.atguigu.gmall.clients;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.otter.canal.client.CanalConnector;
import com.alibaba.otter.canal.client.CanalConnectors;
import com.alibaba.otter.canal.protocol.CanalEntry;
import com.alibaba.otter.canal.protocol.Message;
import com.atguigu.gmall.constants.TopicName;
import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;

import java.net.InetSocketAddress;
import java.util.List;
import java.util.Random;

public class MyClient2 {
    public static void main(String[] args) throws InterruptedException, InvalidProtocolBufferException {
        CanalConnector canalConnector = CanalConnectors.newSingleConnector(new InetSocketAddress("hadoop103", 11111),
                "example", null, null);

        canalConnector.connect();

        canalConnector.subscribe("gmall0726.*");

        while (true){
            Message message = canalConnector.get(100);
                if (message.getId() == -1){

                    System.out.println("没有数据生成");

                    Thread.sleep(6666);

                    continue;
                }
//            System.out.println("接收到的数据：\n"+message);

            List<CanalEntry.Entry> entries = message.getEntries();

            for (CanalEntry.Entry entry: entries) {

                String tableName = entry.getHeader().getTableName();

                if (entry.getEntryType()==CanalEntry.EntryType.ROWDATA){
                    parseData(tableName,entry.getStoreValue());
                }
            }
        }



    }

    private static void parseData(String tableName,ByteString storeValue) throws InvalidProtocolBufferException, InterruptedException {
        CanalEntry.RowChange rowChange = CanalEntry.RowChange.parseFrom(storeValue);

        if (tableName.equals("order_info") && rowChange.getEventType()==CanalEntry.EventType.INSERT){
            sendDataToKafka(rowChange,TopicName.GMALL_ORDER_INFO);
        }else if (tableName.equals("order_detail") && rowChange.getEventType()==CanalEntry.EventType.INSERT){
            sendDataToKafka(rowChange,TopicName.GMALL_ORDER_DETAIL);
        }else if (tableName.equals("user_info") && rowChange.getEventType()==CanalEntry.EventType.INSERT || rowChange.getEventType()==CanalEntry.EventType.UPDATE){
            sendDataToKafka(rowChange,TopicName.GMALL_USER_INFO);
        }
    }

    public static void sendDataToKafka(CanalEntry.RowChange rowChange,String topic) throws InterruptedException {
        List<CanalEntry.RowData> rowDatasList = rowChange.getRowDatasList();

        for (CanalEntry.RowData rowData:rowDatasList) {

            JSONObject jsonObject = new JSONObject();

            List<CanalEntry.Column> afterColumnsList = rowData.getAfterColumnsList();

            for (CanalEntry.Column column:afterColumnsList) {

                jsonObject.put(column.getName(),column.getValue());

            }
            int second=new Random().nextInt(5);

            //Thread.sleep((second*1000));

            MyProducer.sendData(topic,jsonObject.toJSONString());
//                System.out.println(jsonObject);
            MyProducer.sendData(topic,jsonObject.toJSONString());


        }
    }
}

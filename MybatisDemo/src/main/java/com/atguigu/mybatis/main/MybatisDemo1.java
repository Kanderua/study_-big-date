package com.atguigu.mybatis.main;

import com.atguigu.mybatis.bean.Employee;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Smexy on 2021/11/26
 */
public class MybatisDemo1 {

    public static void main(String[] args) throws IOException {


        String resource = "config.xml";

        //读取配置文件，利用读取数据流读取
        InputStream inputStream = Resources.getResourceAsStream(resource);
//        配置中有数据库连接相关配置和sql语句配置

        //基于读取的数据流创建一个SqlSessionFactory
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

        System.out.println(sqlSessionFactory);
        System.out.println("===============");

        //建立连接
        SqlSession sqlSession = sqlSessionFactory.openSession();

//        参数对象的唯一标识和参数对象（1是一个参数对象，就是方法中的id属性，）
        //即使用配置文件中那个标识的sql语句和占位符所需传入的参数
        Employee result = sqlSession.selectOne("facai.selectAnEmp", 1);

        Employee employee = (Employee) result;
        //将获取的结果变成对象展示，需要强转（在开始不知道需要什么类型的情况下使用Object）
        System.out.println(employee);

    }
}

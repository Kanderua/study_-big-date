package org.atguigu.gmall_publisher.service;

import com.alibaba.fastjson.JSONObject;
import org.atguigu.gmall_publisher.bean.DAUDataPerHour;
import org.atguigu.gmall_publisher.bean.GMVDataPerHour;


import java.io.IOException;
import java.util.List;

public interface PublisherService {
    Integer getDAUByDate(String date);

    Integer getNewUserCountByDate(String date);

    List<DAUDataPerHour> getDauDatasPerHour(String date);

    Double getGMVByDate(String date);

    List<GMVDataPerHour> getGMVDatasPerHour(String date);

    JSONObject getSaleDetail(String date,String keyword,Integer startpage,Integer size) throws IOException;
}

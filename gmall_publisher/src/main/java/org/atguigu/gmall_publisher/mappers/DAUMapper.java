package org.atguigu.gmall_publisher.mappers;

import com.baomidou.dynamic.datasource.annotation.DS;
import org.atguigu.gmall_publisher.bean.DAUDataPerHour;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@DS("hbase")
public interface DAUMapper {
    Integer getDAUByDate(String date);

    Integer getNewUserCountByDate(String date);

    List<DAUDataPerHour> getDauDatasPerHour(String date);
}

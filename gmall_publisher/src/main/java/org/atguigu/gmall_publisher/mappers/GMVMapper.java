package org.atguigu.gmall_publisher.mappers;

import com.baomidou.dynamic.datasource.annotation.DS;
import org.atguigu.gmall_publisher.bean.GMVDataPerHour;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@DS("mysql")
public interface GMVMapper {
    Double getGMVByDate(String date);

    List<GMVDataPerHour> getGMVDatasPerHour(String date);
}
